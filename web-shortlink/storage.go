package main

type Storage interface {
	Shorten(url string, exp int64) (string, error)
	ShortlinkInfo(eid string) (interface{}, error) // 传入短地址，返回短地址对应的信息，是一个空接口
	Unshorten(eid string) (string, error)          // 短地址转化为原来的长地址
}
