package csvs

import "daily-golang-package/yuanshen-demo/server/src/utils"

type ConfigRole struct {
	RoleId          int    `json:"RoleId"`
	ItemName        string `json:"ItemName"`
	Star            int    `json:"Star"`
	Stuff           int    `json:"Stuff"`
	StuffNum        int    `json:"StuffNum"`
	StuffItem       int    `json:"StuffItem"`
	StuffItemNum    int    `json:"StuffItemNum"`
	MaxStuffItem    int    `json:"MaxStuffItem"`
	MaxStuffItemNum int    `json:"MaxStuffItemNum"`
	Type            int    `json:"Type"`
}

var ConfigRoleMap map[int]*ConfigRole

func init() {
	ConfigRoleMap = make(map[int]*ConfigRole)
	utils.GetCsvUtilMgr().LoadCsv("Role", ConfigRoleMap)
	return
}

func GetRoleConfig(roleId int) *ConfigRole {
	return ConfigRoleMap[roleId]
}
