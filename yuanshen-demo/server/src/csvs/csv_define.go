package csvs

const (
	LOGIC_FALSE = 0
	LOGIC_TRUE  = 1
	PERCENT_ALL = 10000
)

const (
	DROP_ITEM_TYPE_ITEM   = 1
	DROP_ITEM_TYPE_GROUP  = 2
	DROP_ITEM_TYPE_WEIGHT = 3
)

const (
	EVENT_START  = 0
	EVENT_FINISH = 9
	EVENT_END    = 10

	EVENT_TYPE_NORMAL = 1
	EVENT_TYPE_REWORD = 2
)

const (
	MAP_REFRESH_DAY  = 1
	MAP_REFRESH_WEEK = 2
	MAP_REFRESH_SELF = 3
	MAP_REFRESH_CANT = 4

	MAP_REFRESH_DAY_TIME  = 20
	MAP_REFRESH_WEEK_TIME = 40
	MAP_REFRESH_SELF_TIME = 60

	REFRESH_SYSTEM = 1
	REFRESH_PLAYER = 2
)

const (
	REDUCE_WORLD_LEVEL_START         = 5    // 降低世界等级的要求
	REDUCE_WORLD_LEVEL_MAX           = 1    // 最多能降低多少级
	REDUCE_WORLD_LEVEL_COOL_TIME     = 20   // 冷却时间
	SHOW_SIZE                        = 9    // 可展示最大数量
	ADD_ROLE_TIME_NORMAL_MIN         = 2    // 获取角色最小次数
	ADD_ROLE_TIME_NORMAL_MAX         = 7    //获取角色最大次数
	WEAPON_MAX_COUNT                 = 2000 // 武器最大数量
	WEAPON_MAX_REFINE                = 5    //武器的最大精炼等级
	RELICS_MAX_COUNT                 = 1500 //圣遗物最大数量
	FIVE_STAR_TIMES_LIMIT            = 73   // 五星出现最大次数
	FIVE_STAR_TIMES_LIMIT_EACH_VALUE = 600
	FOUR_STAR_TIMES_LIMIT            = 8 // 四星出现最大次数
	FOUR_STAR_TIMES_LIMIT_EACH_VALUE = 5100
	ALL_ENTRY_RATE                   = 2000
)
