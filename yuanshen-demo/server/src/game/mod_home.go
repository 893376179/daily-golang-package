package game

import (
	"daily-golang-package/yuanshen-demo/server/src/csvs"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type HomeItem struct {
	HomeItemId  int
	HomeItemNum int64
	KeyId       int
}

type ModHome struct {
	HomeItemIdInfo map[int]*HomeItem

	player *Player
	path   string
}

func (self *ModHome) AddItem(itemId int, num int64) {
	_, ok := self.HomeItemIdInfo[itemId]
	if ok {
		self.HomeItemIdInfo[itemId].HomeItemNum += num
	} else {
		self.HomeItemIdInfo[itemId] = &HomeItem{HomeItemId: itemId, HomeItemNum: num}
	}
	config := csvs.GetItemConfig(itemId)
	if config != nil {
		fmt.Println("获得家具物品", config.ItemName, "----数量：", num, "----当前数量：", self.HomeItemIdInfo[itemId].HomeItemNum)
	}
}

func (self *ModHome) SaveData() {
	content, err := json.Marshal(self)
	if err != nil {
		return
	}
	err = ioutil.WriteFile(self.path, content, os.ModePerm)
	if err != nil {
		return
	}
}

func (self *ModHome) LoadData(player *Player) {
	self.player = player
	self.path = self.player.localPath + "./yuanshen-demo/save/home.json"
	configFile, err := ioutil.ReadFile(self.path)
	if err != nil {
		return
	}
	err = json.Unmarshal(configFile, &self)
	if err != nil {
		self.InitData()
		return
	}
	if self.HomeItemIdInfo == nil {
		self.HomeItemIdInfo = make(map[int]*HomeItem)
	}
	return
}

func (self *ModHome) InitData() {
}
