package game

import (
	"daily-golang-package/yuanshen-demo/server/src/csvs"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Relics struct {
	RelicsId   int
	KeyId      int
	MainEntry  int
	level      int
	Exp        int
	OtherEntry []int
	RoleId     int
}

type ModRelics struct {
	RelicsInfo map[int]*Relics
	MaxKey     int

	player *Player
	path   string
}

func (self *ModRelics) AddItem(itemId int, num int64) {
	config := csvs.GetRelicsConfig(itemId)
	if config == nil {
		fmt.Println("配置不存在")
		return
	}
	if len(self.RelicsInfo)+int(num) > csvs.RELICS_MAX_COUNT {
		fmt.Println("超过最大值")
		return
	}

	for i := int64(0); i < num; i++ {
		relics := self.NewRelics(itemId)
		self.RelicsInfo[relics.KeyId] = relics
		fmt.Println("获得圣遗物:")
		relics.ShowInfo()
	}
}

func (self *ModRelics) NewRelics(itemId int) *Relics {
	relicsRel := new(Relics)
	relicsRel.RelicsId = itemId
	self.MaxKey += 1
	relicsRel.KeyId = self.MaxKey
	config := csvs.ConfigRelicsMap[itemId]
	if config == nil {
		return nil
	}
	return relicsRel
}

func (self *Relics) ShowInfo() {
	fmt.Println(fmt.Sprintf("key: %d,Id:%d", self.KeyId, self.RelicsId))
}

func (self *ModRelics) SaveData() {
	content, err := json.Marshal(self)
	if err != nil {
		return
	}
	err = ioutil.WriteFile(self.path, content, os.ModePerm)
	if err != nil {
		return
	}
}

func (self *ModRelics) RelicsUp(player *Player) {

}

func (self *ModRelics) RelicsTop(player *Player) {}

func (self *ModRelics) RelicsTestBest(player *Player) {}

func (self *ModRelics) LoadData(player *Player) {
	self.player = player
	self.path = self.player.localPath + "./yuanshen-demo/save/relics.json"
	configFile, err := ioutil.ReadFile(self.path)
	if err != nil {
		self.InitData()
		return
	}
	err = json.Unmarshal(configFile, &self)
	if err != nil {
		self.InitData()
		return
	}
	if self.RelicsInfo == nil {
		self.RelicsInfo = make(map[int]*Relics)
	}
	return
}

func (self *ModRelics) InitData() {
	if self.RelicsInfo == nil {
		self.RelicsInfo = make(map[int]*Relics)
	}
}
