package game

import (
	"daily-golang-package/yuanshen-demo/server/src/csvs"
	"fmt"
	"regexp"
	"time"
)

var manageBanWord *ManageBanWord

type ManageBanWord struct {
	BanWordBase  []string
	BanWordExtra []string
	MsgChannel   chan int
}

func GetManageBanWord() *ManageBanWord {
	if manageBanWord == nil {
		manageBanWord = new(ManageBanWord)
		manageBanWord.BanWordBase = []string{"外挂", "工具"}
		manageBanWord.BanWordExtra = []string{"翻墙"}
	}
	return manageBanWord
}

func (self *ManageBanWord) IsBanWord(txt string) bool {
	for _, v := range self.BanWordBase {
		match, _ := regexp.MatchString(v, txt)
		if match {
			fmt.Println("发现违禁词:", v)
			return match
		}
	}
	for _, v := range self.BanWordExtra {
		match, _ := regexp.MatchString(v, txt)
		// fmt.Println(match, v)
		if match {
			fmt.Println("发现违禁词:", v)
			return match
		}
	}
	return false
}

func (self *ManageBanWord) Run() {
	GetServer().AddGo()
	self.BanWordBase = csvs.GetBanWordBase()
	// 基础词库更新
	ticker := time.NewTicker(time.Second * 1)
	for {
		select {
		case <-ticker.C:
			if time.Now().Unix()%10 == 0 {
				// fmt.Println("更新词库")
				GetServer().UpdateBanword(self.BanWordBase)
			} else {
				// fmt.Println("待机")
			}
		case _, ok := <-self.MsgChannel:
			if !ok {
				GetServer().GoDone()
				return
			}
		}
	}
}

func (self *ManageBanWord) Close() {
	close(self.MsgChannel)
}
