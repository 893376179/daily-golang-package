package game

import (
	"daily-golang-package/yuanshen-demo/server/src/csvs"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type DBConfig struct {
	DBUser     string `json:"dbuser"`
	DBPassword string `json:"dbpassword"`
}

type ServerConfig struct {
	ServerId      int       `json:"serverid"`
	Host          string    `json:"host"`
	LocalSavePath string    `json:"localsavepath"`
	DBConfig      *DBConfig `json:"dbconfig"`
	HttpHost      string    `json:"httphost"`
}

type Server struct {
	Wait        sync.WaitGroup
	BanWordBase []string
	Lock        *sync.RWMutex

	Config *ServerConfig
}

var server *Server

func GetServer() *Server {
	if server == nil {
		server = new(Server)
		server.Lock = new(sync.RWMutex)
	}
	return server
}

func (self *Server) Start() {
	// 读取全局配置
	self.LoadConfig()
	// 加载配置
	rand.Seed(time.Now().Unix())
	csvs.CheckLoadCsv()
	go GetManageBanWord().Run()
	go GetManageHttp().InitData()
	go GetManagePlayer().Run()

	playerTest := NewTestPlayer(nil, 10000666)
	go playerTest.Run()
	go self.SignalHandle()

	http.ListenAndServe(GetServer().Config.Host, nil)
	self.Wait.Wait()
	fmt.Println("服务器关闭成功")
}

func (self *Server) LoadConfig() {
	configFile, err := ioutil.ReadFile("./yuanshen-demo/config.json")
	if err != nil {
		fmt.Println("error", err)
		return
	}
	err = json.Unmarshal(configFile, &self.Config)
	if err != nil {
		fmt.Println("error")
		return
	}
	return
}

func (self *Server) SignalHandle() {
	channelSignal := make(chan os.Signal)
	signal.Notify(channelSignal, syscall.SIGINT)
	for {
		select {
		case <-channelSignal:
			fmt.Println("get syscall.SIGINT")
			fmt.Println("get syscall.SIGINT")
			fmt.Println("get syscall.SIGINT")
			fmt.Println("get syscall.SIGINT")
			fmt.Println("get syscall.SIGINT")
			fmt.Println("get syscall.SIGINT")
			fmt.Println("get syscall.SIGINT")
			fmt.Println("get syscall.SIGINT")
			self.Close()
		}
	}
}

func (self *Server) Close() {
	GetManageBanWord()
}

func (self *Server) AddGo() {
	self.Wait.Add(1)
}

func (self *Server) GoDone() {
	self.Wait.Done()
}

func (self *Server) UpdateBanword(banword []string) {
	self.Lock.Lock()
	defer self.Lock.Unlock()
	self.BanWordBase = banword
}
