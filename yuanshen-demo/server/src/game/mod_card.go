package game

import (
	"daily-golang-package/yuanshen-demo/server/src/csvs"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Card struct {
	CardId       int
	Friendliness int
}

type ModCard struct {
	CardInfo map[int]*Card
	player   *Player
	path     string
}

func (self *ModCard) IsHasCard(cardId int) bool {
	_, ok := self.CardInfo[cardId]
	return ok
}

func (self *ModCard) AddItem(itemId int, friendliness int) {
	_, ok := self.CardInfo[itemId]
	if ok {
		fmt.Println("已存在卡片", itemId)
		return
	}
	config := csvs.GetCardConfig(itemId)
	if config == nil {
		fmt.Println("非法头像", itemId)
		return
	}
	if friendliness < config.Friendliness {
		fmt.Println("好感度不足: ", friendliness)
		return
	}
	self.CardInfo[itemId] = &Card{CardId: itemId}
	fmt.Println("获得卡片", itemId)
}

func (self *ModCard) CheckGetCard(roleId int, friendiness int) {
	config := csvs.GetCardConfigByRoleId(roleId)
	if config == nil {
		return
	}
	self.AddItem(config.CardId, friendiness)
}

func (self *ModCard) SaveData() {
	content, err := json.Marshal(self)
	if err != nil {
		return
	}
	err = ioutil.WriteFile(self.path, content, os.ModePerm)
	if err != nil {
		return
	}
}

func (self *ModCard) LoadData(player *Player) {
	self.player = player
	self.path = self.player.localPath + "./yuanshen-demo/save/card.json"
	configFile, err := ioutil.ReadFile(self.path)
	if err != nil {
		return
	}
	err = json.Unmarshal(configFile, &self)
	if err != nil {
		self.InitData()
		return
	}
	if self.CardInfo == nil {
		self.CardInfo = make(map[int]*Card)
	}
	return
}

func (self *ModCard) InitData() {
	if self.CardInfo == nil {
		self.CardInfo = make(map[int]*Card)
	}
}
