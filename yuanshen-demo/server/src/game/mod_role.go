package game

import (
	"daily-golang-package/yuanshen-demo/server/src/csvs"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type RoleInfo struct {
	RoleId     int
	GetTimes   int
	RelicsInfo []int
	WeaponInfo int
}

type ModRole struct {
	RoleInfo map[int]*RoleInfo

	player *Player
	path   string
}

func (self *ModRole) IsHasRole(roleId int) bool {
	return true
}

func (self *ModRole) GetRoleId(roleId int) int {
	return 80
}

func (self *ModRole) GetRoleLevel(roleId int) int {
	return 100
}

func (self *ModRole) AddItem(roleId int, num int64) {
	config := csvs.GetRoleConfig(roleId)
	if config == nil {
		fmt.Println("配置不存在roleId: ", roleId)
		return
	}
	for i := 0; i < int(num); i++ {
		_, ok := self.RoleInfo[roleId]
		if !ok {
			data := new(RoleInfo)
			data.RoleId = roleId
			data.GetTimes = 1
			self.RoleInfo[roleId] = data
		} else {
			fmt.Println("获得实际物品: ")
			self.RoleInfo[roleId].GetTimes += 1
			if self.RoleInfo[roleId].GetTimes >= csvs.ADD_ROLE_TIME_NORMAL_MIN &&
				self.RoleInfo[roleId].GetTimes <= csvs.ADD_ROLE_TIME_NORMAL_MAX {
				player.GetModBag().AddItemToBag(config.MaxStuffItem, int64(config.MaxStuffItemNum))
			}
		}
	}
	itemConfig := csvs.GetItemConfig(roleId)
	if itemConfig != nil {
		fmt.Println("获得角色: ", itemConfig.ItemName, "-----次数: ", self.RoleInfo[roleId].GetTimes, "次")
	}
}

func (self *ModRole) HandleSendRoleInfo(player *Player) {
	fmt.Println(fmt.Sprintf("当前拥有角色信息如下:"))
	for _, v := range self.RoleInfo {
		v.SendRoleInfo(player)
	}
}

func (self *RoleInfo) SendRoleInfo(player *Player) {
	fmt.Println(fmt.Sprintf("%s:,Id:%d,累计获得次数:%d", csvs.GetItemName(self.RoleId), self.RoleId, self.GetTimes))
	self.ShowInfo(player)
}

func (self *RoleInfo) ShowInfo(player *Player) {
	fmt.Println(fmt.Sprintf("当前角色:%s,角色ID:%d", csvs.GetItemName(self.RoleId), self.RoleId))
	weaponNow := player.GetModWeapon().WeaponInfo[self.WeaponInfo]
	if weaponNow == nil {
		fmt.Println(fmt.Sprintf("武器:未穿戴"))
	} else {
		fmt.Println(fmt.Sprintf("武器:%s,key:%d", csvs.GetItemName(weaponNow.WeaponId), self.WeaponInfo))
	}
	suitMap := make(map[int]int)
	for _, v := range self.RelicsInfo {
		reclisNow := player.GetModRelics().RelicsInfo[v]
		if reclisNow == nil {
			fmt.Println(fmt.Sprintf("未穿戴"))
			continue
		}
		fmt.Println(fmt.Sprintf("%s,key:%d", csvs.GetItemName(reclisNow.RelicsId), v))
		reclisNowConfig := csvs.GetRelicsConfig(reclisNow.RelicsId)
		if reclisNowConfig != nil {
			suitMap[reclisNowConfig.Type]++
		}
	}
	// suitSkill := make([]int, 0)
	// for suit, num := range suitMap {
	// 	for _, config := range csvs.ConfigRelicsSuitMap[suit] {
	// 		if num >= config.Num {
	// 			suitSkill = append(suitSkill, config.SuitSkill)
	// 		}
	// 	}
	// }
	// for _, v := range suitSkill {
	// 	fmt.Println(fmt.Sprintf("激活套装效果:%d", v))
	// }
}

func (self *ModRole) GetRoleInfoForPoolCheck() (map[int]int, map[int]int) {
	fiveInfo := make(map[int]int)
	fourInfo := make(map[int]int)
	for _, v := range self.RoleInfo {
		roleConfig := csvs.GetRoleConfig(v.RoleId)
		if roleConfig == nil {
			continue
		}
		if roleConfig.Star == 5 {
			fiveInfo[roleConfig.RoleId] = v.GetTimes
		} else if roleConfig.Star == 4 {
			fourInfo[roleConfig.RoleId] = v.GetTimes
		}
	}
	return fiveInfo, fourInfo
}

func (self *ModRole) SaveData() {
	content, err := json.Marshal(self)
	if err != nil {
		return
	}
	err = ioutil.WriteFile(self.path, content, os.ModePerm)
	if err != nil {
		return
	}
}

func (self *ModRole) LoadData(player *Player) {
	self.player = player
	self.path = self.player.localPath + "./yuanshen-demo/save/role.json"
	configFile, err := ioutil.ReadFile(self.path)
	if err != nil {
		self.InitData()
		return
	}
	err = json.Unmarshal(configFile, &self)
	if err != nil {
		self.InitData()
		return
	}
	if self.RoleInfo == nil {
		self.RoleInfo = make(map[int]*RoleInfo)
	}
	return
}

func (self *ModRole) InitData() {
	if self.RoleInfo == nil {
		self.RoleInfo = make(map[int]*RoleInfo)
	}
}
