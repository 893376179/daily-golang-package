package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"golang.org/x/sync/singleflight"
)

var count int32

func main() {
	var wg sync.WaitGroup
	now := time.Now()
	sg := &singleflight.Group{}
	for i := 0; i < 1; i++ {
		wg.Add(1)
		go func() {
			// Getcontent(1)
			SingleGetcontent(sg, 2)
			wg.Done()
		}()
	}
	wg.Wait()

	fmt.Printf("耗时：%s", time.Since(now))
}

func Getcontent(id int) (string, error) {
	atomic.AddInt32(&count, 1)
	time.Sleep(time.Duration(count) * time.Millisecond)
	return fmt.Sprintf("获取第 %d 个内容", id), nil
}

func SingleGetcontent(sg *singleflight.Group, id int) (string, error) {
	v, err, ok := sg.Do(fmt.Sprintf("%d", id), func() (interface{}, error) {
		return Getcontent(id)
	})
	fmt.Println(ok)
	return v.(string), err
}
