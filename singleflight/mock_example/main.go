package main

import (
	"errors"
	"log"
	"sync"

	"golang.org/x/sync/singleflight"
)

var errNotExist = errors.New("not exist")
var g singleflight.Group

func main() {
	var wg sync.WaitGroup
	wg.Add(100)
	for i := 0; i < 100; i++ {
		go func() {
			defer wg.Done()
			data, err := getData("key")
			if err != nil {
				log.Print(err)
				return
			}
			log.Println(data)
		}()
	}
	wg.Wait()
}

func getData(key string) (string, error) {
	data, err := getDataFromCache(key)
	if err == errNotExist {
		// data, err = getDataFromDB(key)
		// if err != nil {
		// 	log.Println(err)
		// 	return "", err
		// }
		v, err, _ := g.Do(key, func() (interface{}, error) {
			return getDataFromDB(key)
		})
		if err != nil {
			log.Println(err)
			return "", err
		}
		data = v.(string)
	} else if err != nil {
		return "", err
	}
	return data, nil
}

func getDataFromCache(key string) (string, error) {
	return "", errNotExist
}

func getDataFromDB(key string) (string, error) {
	log.Printf("get %s from database", key)
	return "data", nil
}
