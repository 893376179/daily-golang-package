package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// VirtualMachine根据CRD定义的结构体
type VirtualMachine struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              VMSpec `json:"spec"`
}

type VMSpec struct {
	UUId   string `json:"uuid"`
	Name   string `json:"name"`
	Image  string `json:"image"`
	Memory int    `json:"memory"`
	Disk   int    `json:"disk"`
}

// VirtualMachineList 资源列表
type VirtualMachineList struct {
	metav1.TypeMeta `json:",inline"`
	// 标准的list metadata
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []VirtualMachine `json:"items"`
}
