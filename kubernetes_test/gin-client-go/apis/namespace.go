package apis

import (
	"daily-golang-package/kubernetes_test/gin-client-go/service"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetNamespaces(c *gin.Context) {
	namespaces, err := service.GetNamespaces()
	fmt.Println("namespaces:", namespaces)
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
	}
	c.JSON(http.StatusOK, namespaces)
}
