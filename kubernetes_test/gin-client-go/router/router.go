package router

import (
	"daily-golang-package/kubernetes_test/gin-client-go/apis"
	"daily-golang-package/kubernetes_test/gin-client-go/middleware"

	"github.com/gin-gonic/gin"
)

func InitRouter(r *gin.Engine) {
	middleware.InitMiddleware(r)
	r.GET("/ping", apis.Ping)
	r.GET("/namespace", apis.GetNamespaces)
	r.GET("/namespace/:namespaceName/pods", apis.GetPods)
	r.GET("/namespace/:namespaceName/pod/:podName/container/:containerName", apis.ExecContainer)
}
