package service

import (
	"context"
	"daily-golang-package/kubernetes_test/gin-client-go/client"
	"encoding/json"
	"errors"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/remotecommand"
	"k8s.io/klog/v2"
)

func GetPods(namespaceName string) ([]v1.Pod, error) {
	ctx := context.Background()
	clientSet, err := client.GetK8sClientSet()
	if err != nil {
		klog.Error(err)
		return nil, err
	}
	podsList, err := clientSet.CoreV1().Pods(namespaceName).List(ctx, metav1.ListOptions{})
	if err != nil {
		klog.Error(err)
		return nil, err
	}
	return podsList.Items, nil
}

type WsMessage struct {
	MessageType int
	Data        []byte
}

type WsConnection struct {
	wsSocket  *websocket.Conn
	inChan    chan *WsMessage
	outChan   chan *WsMessage
	mutex     sync.Mutex // 为了防止channel重复关闭
	isClosed  bool
	closeChan chan byte // 用来通知关闭的channel
}

func (wsConn *WsConnection) WsClose() {
	err := wsConn.wsSocket.Close()
	if err != nil {
		klog.Errorln(err)
		return
	}
	wsConn.mutex.Lock()
	defer wsConn.mutex.Unlock()
	if !wsConn.isClosed {
		wsConn.isClosed = true
		close(wsConn.closeChan)
	}
}

//读消息
func (wsConn *WsConnection) wsReadLoop() {
	var (
		msgType int
		data    []byte
		msg     *WsMessage
		err     error
	)
	for {
		if msgType, data, err = wsConn.wsSocket.ReadMessage(); err != nil {
			goto ERROR
		}
		msg = &WsMessage{
			MessageType: msgType,
			Data:        data,
		}
		select {
		// 将消息写入到channel中
		case wsConn.inChan <- msg:
			// 如果说websocket已关闭，就让函数直接退出
		case <-wsConn.closeChan:
			goto CLOSED
		}
	}
ERROR:
	wsConn.WsClose()
CLOSED:
}

// 写消息
func (wsConn *WsConnection) wsWriteLoop() {
	var (
		msg *WsMessage
		err error
	)
	for {
		select {
		case msg = <-wsConn.outChan:
			if err = wsConn.wsSocket.WriteMessage(msg.MessageType, msg.Data); err != nil {
				goto ERROR
			}
		case <-wsConn.closeChan:
			goto CLOSED
		}
	}
ERROR:
	wsConn.WsClose()
CLOSED:
}

// 发送消息
func (wsConn *WsConnection) WsWrite(messageType int, data []byte) (err error) {
	select {
	case wsConn.outChan <- &WsMessage{MessageType: messageType, Data: data}:
		return
	case <-wsConn.closeChan:
		err = errors.New("websocket closed")
	}
	return
}

// 读取消息
func (wsConn *WsConnection) WsRead() (msg *WsMessage, err error) {
	select {
	case msg = <-wsConn.inChan:
		return
	case <-wsConn.closeChan:
		err = errors.New("websocket closed")
	}
	return
}

// 处理webSocket跨域请求
var wsUpgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func InitWebsocket(resp http.ResponseWriter, req *http.Request) (wsConn *WsConnection, err error) {
	var (
		wsSocket *websocket.Conn
	)
	if wsSocket, err = wsUpgrader.Upgrade(resp, req, nil); err != nil {
		klog.Error("init webSocket failure")
	} else {
		wsConn = &WsConnection{
			wsSocket:  wsSocket,
			inChan:    make(chan *WsMessage, 1000),
			outChan:   make(chan *WsMessage, 1000),
			isClosed:  false,
			closeChan: make(chan byte),
		}
	}
	// 读取协程
	go wsConn.wsReadLoop()
	// 写协程
	go wsConn.wsWriteLoop()
	return
}

type streamHandler struct {
	WsConn      *WsConnection
	resizeEvent chan remotecommand.TerminalSize
}

func (handler *streamHandler) Write(p []byte) (size int, err error) {
	copyData := make([]byte, len(p))
	copy(copyData, p)
	size = len(p)
	err = handler.WsConn.WsWrite(websocket.TextMessage, copyData)
	return
}

type xtermMessage struct {
	MsgType string `json:"type"`
	Input   string `json:"input"`
	Rows    uint16 `json:"rows"`
	Cols    uint16 `json:"cols"`
}

// 读取web端输入的数据，将websocket转成stream流
func (handler *streamHandler) Read(p []byte) (size int, err error) {
	var (
		xtermMsg xtermMessage
		msg      *WsMessage
	)
	msg, err = handler.WsConn.WsRead()
	if err != nil {
		klog.Errorln(err)
		return
	}
	// 解析
	if err = json.Unmarshal(msg.Data, &xtermMsg); err != nil {
		return
	}
	if xtermMsg.MsgType == "resize" {
		handler.resizeEvent <- remotecommand.TerminalSize{Width: xtermMsg.Cols, Height: xtermMsg.Rows}
	} else if xtermMsg.MsgType == "input" {
		size = len(xtermMsg.Input)
		copy(p, xtermMsg.Input)
	}
	return
}

func (handler *streamHandler) Next() (size *remotecommand.TerminalSize) {
	ret := <-handler.resizeEvent
	size = &ret
	return
}

func WebSSH(namespaceName string, podName string, containerName string, method string, resp http.ResponseWriter, req *http.Request) error {
	ctx := context.Background()
	config, err := client.GetResetConfig()
	if err != nil {
		klog.Errorln(err)
		return err
	}
	clientSet, err := client.GetK8sClientSet()
	if err != nil {
		klog.Errorln(err)
		return err
	}
	reqSSH := clientSet.CoreV1().RESTClient().Post().Resource("pods").Name(podName).Namespace(namespaceName).SubResource("exec").
		VersionedParams(&v1.PodExecOptions{
			Stdin:     true,
			Stdout:    true,
			Stderr:    true,
			TTY:       true,
			Container: containerName,
			Command:   []string{method},
		}, scheme.ParameterCodec)
	excutor, err := remotecommand.NewSPDYExecutor(config, "POST", reqSSH.URL())
	if err != nil {
		klog.Errorln(err)
		return err
	}
	wsConn, err := InitWebsocket(resp, req)
	if err != nil {
		klog.Errorln(err)
		return err
	}
	handler := &streamHandler{
		WsConn:      wsConn,
		resizeEvent: make(chan remotecommand.TerminalSize),
	}
	if err := excutor.StreamWithContext(ctx, remotecommand.StreamOptions{
		Stdin:             handler,
		Stdout:            handler,
		Stderr:            handler,
		Tty:               true,
		TerminalSizeQueue: handler,
	}); err != nil {
		goto END
	}
	return err
END:
	klog.Errorln(err)
	wsConn.WsClose()
	return err
}
