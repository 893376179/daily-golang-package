package main

import (
	"fmt"
	"time"

	v1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/klog/v2"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

func main() {
	conf, err := config.GetConfig()
	if err != nil {
		panic(err)
	}
	clientset, err := kubernetes.NewForConfig(conf)
	if err != nil {
		panic(err)
	}
	informerFactory := informers.NewSharedInformerFactory(clientset, 30*time.Second)
	deploymentInformer := informerFactory.Apps().V1().Deployments()
	informer := deploymentInformer.Informer()
	deployLister := deploymentInformer.Lister()
	_, err = informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    onAdd,
		UpdateFunc: onUpdate,
		DeleteFunc: onDelete,
	})
	if err != nil {
		panic(err)
	}
	stopper := make(chan struct{})
	defer close(stopper)
	// 启动 informer List & watch
	informerFactory.Start(stopper)
	// 等待所有的Informer缓存同步
	informerFactory.WaitForCacheSync(stopper)
	deployments, err := deployLister.Deployments("demo").List(labels.Everything())
	// 遍历deploy列表
	for index, deploy := range deployments {
		fmt.Printf("%d -> %s\n", index, deploy.Name)
	}
	<-stopper
}

func onAdd(obj any) {
	deploy := obj.(*v1.Deployment)
	klog.Infoln("add a deploy: ", deploy.Name)
}

func onUpdate(oldObj, newObj any) {
	oldDeploy := oldObj.(*v1.Deployment)
	newDeploy := newObj.(*v1.Deployment)
	klog.Infoln("update deploy: ", oldDeploy.Spec.Replicas, "-->", newDeploy.Spec.Replicas)
}

func onDelete(obj any) {
	deploy := obj.(*v1.Deployment)
	klog.Infoln("delete a deploy: ", deploy.Name)
}
