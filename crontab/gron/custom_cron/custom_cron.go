package main

import (
	"fmt"
	"sync"

	"github.com/roylee0704/gron"
	"github.com/roylee0704/gron/xtime"
)

type GreetingJob struct {
	Msg string
}

func (job GreetingJob) Run() {
	fmt.Println(job.Msg)
}

func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	g1 := GreetingJob{Msg: "Hello world1"}
	g2 := GreetingJob{Msg: "Hello world2"}

	c := gron.New()
	c.Add(gron.Every(2*xtime.Second), g1)
	c.Add(gron.Every(5*xtime.Second), g2)
	c.Start()
	wg.Wait()
}
