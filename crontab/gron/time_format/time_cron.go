package main

import (
	"fmt"
	"sync"
	"time"

	"github.com/roylee0704/gron"
	"github.com/roylee0704/gron/xtime"
)

func main() {
	var (
		// schedules
		daily   = gron.Every(1 * xtime.Day).At("00:00")
		weekly  = gron.Every(1 * xtime.Week).At("23:59")
		monthly = gron.Every(30 * xtime.Day)
	)
	var wg sync.WaitGroup
	wg.Add(1)

	c := gron.New()
	// c.AddFunc(gron.Every(1*xtime.Second), func() { fmt.Println("runs every second") })
	c.AddFunc(gron.Every(1*xtime.Minute), func() { fmt.Println("runs every minute") })
	c.AddFunc(gron.Every(1*xtime.Hour), func() { fmt.Println("runs every hour") })
	c.AddFunc(daily, func() { fmt.Println("runs every day") })
	c.AddFunc(weekly, func() { fmt.Println("runs every week") })
	c.AddFunc(monthly, func() { fmt.Println("runs every month") })
	t, _ := time.ParseDuration("1m10s")
	c.AddFunc(gron.Every(t), func() { fmt.Println("runs every 1 minutes 10 seconds.") })

	c.Start()
	wg.Wait()
}
