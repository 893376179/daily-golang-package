package main

import (
	"fmt"
	"time"

	"github.com/robfig/cron/v3"
)

func main() {
	c := cron.New()
	c.AddFunc("30 * * * *", func() {
		fmt.Println("Every hour on the half hour")
	})
	c.AddFunc("30 3-6,20-23 * * *", func() {
		fmt.Println("On the half hour of 3-6am, 8-11pm")
	})
	c.AddFunc("0 0 1 1 *", func() {
		fmt.Println("Jan 1 every year")
	})

	c.AddFunc("@hourly", func() {
		fmt.Println("Every hour")
	})
	c.AddFunc("@daily", func() {
		fmt.Println("Every day")
	})
	c.AddFunc("@weekly", func() {
		fmt.Println("Every week")
	})

	c.Start()

	for {
		time.Sleep(time.Second)
	}
}
