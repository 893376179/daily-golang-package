package main

import (
	"fmt"
	"time"

	"github.com/robfig/cron/v3"
)

type GreetingJob struct {
	Msg string
}

func (g GreetingJob) Run() {
	fmt.Println("Hello " + g.Msg)
}

func main() {
	c := cron.New()
	c.AddJob("@every 1s", GreetingJob{"wolrd"})
	c.Start()
	time.Sleep(3 * time.Second)
}

// Hello world
// Hello world
// Hello world
