package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/robfig/cron/v3"
)

func main() {
	c := cron.New(
		cron.WithLogger(
			cron.VerbosePrintfLogger(log.New(os.Stdout, "cron: ", log.LstdFlags))))
	c.AddFunc("@every 2s", func() {
		fmt.Println("hello world")
	})
	c.Start()

	time.Sleep(5 * time.Second)
}

// cron: 2022/10/11 19:13:05 start
// cron: 2022/10/11 19:13:05 schedule, now=2022-10-11T19:13:05+08:00, entry=1, next=2022-10-11T19:13:07+08:00
// cron: 2022/10/11 19:13:07 wake, now=2022-10-11T19:13:07+08:00
// cron: 2022/10/11 19:13:07 run, now=2022-10-11T19:13:07+08:00, entry=1, next=2022-10-11T19:13:09+08:00
// hello world
// cron: 2022/10/11 19:13:09 wake, now=2022-10-11T19:13:09+08:00
// hello world
// cron: 2022/10/11 19:13:09 run, now=2022-10-11T19:13:09+08:00, entry=1, next=2022-10-11T19:13:11+08:00
