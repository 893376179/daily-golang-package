package main

import (
	"log"
	"time"

	"github.com/robfig/cron/v3"
)

type delayJob struct {
	count int
}

func (job *delayJob) Run() {
	time.Sleep(2 * time.Second)
	job.count++
	log.Printf("%d: hello world\n", job.count)
}

func main() {
	c := cron.New()
	c.AddJob("@every 1s", cron.NewChain(cron.DelayIfStillRunning(cron.DefaultLogger)).Then(&delayJob{}))
	c.Start()
	time.Sleep(10 * time.Second)
}

// 2022/10/11 21:22:18 1: hello world
// 2022/10/11 21:22:20 2: hello world
// 2022/10/11 21:22:22 3: hello world
// 2022/10/11 21:22:24 4: hello world
