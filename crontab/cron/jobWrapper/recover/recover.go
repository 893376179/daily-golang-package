package main

import (
	"fmt"
	"time"

	"github.com/robfig/cron/v3"
)

type panicJob struct {
	count int
}

func (job *panicJob) Run() {
	job.count++
	if job.count == 1 {
		panic("oooooooooops!")
	}
	fmt.Println("hello world")
}

func main() {
	c := cron.New()
	c.AddJob("@every 1s", cron.NewChain(cron.Recover(cron.DefaultLogger)).Then(&panicJob{}))
	c.Start()

	time.Sleep(5 * time.Second)
}

// cron: 2022/10/11 21:08:21 panic, error=oooooooooops!, stack=...
// goroutine 7 [running]:
// github.com/robfig/cron/v3.Recover.func1.1.1()
// 	/Users/apple/go/pkg/mod/github.com/robfig/cron/v3@v3.0.1/chain.go:45 +0x85
// panic({0x10a69a0, 0x10d8a80})
// 	/usr/local/Cellar/go/1.17.5/libexec/src/runtime/panic.go:1038 +0x215
// main.(*panicJob).Run(0xedad761c4)
// 	/Users/apple/Desktop/daily-golang-package/crontab/cron/jobWrapper/recover/recover.go:17 +0x85
// github.com/robfig/cron/v3.Recover.func1.1()
// 	/Users/apple/go/pkg/mod/github.com/robfig/cron/v3@v3.0.1/chain.go:53 +0x73
// github.com/robfig/cron/v3.FuncJob.Run(0x0)
// 	/Users/apple/go/pkg/mod/github.com/robfig/cron/v3@v3.0.1/cron.go:136 +0x1a
// github.com/robfig/cron/v3.(*Cron).startJob.func1()
// 	/Users/apple/go/pkg/mod/github.com/robfig/cron/v3@v3.0.1/cron.go:312 +0x6a
// created by github.com/robfig/cron/v3.(*Cron).startJob
// 	/Users/apple/go/pkg/mod/github.com/robfig/cron/v3@v3.0.1/cron.go:310 +0xb2
// hello world
// hello world
// hello world
// hello world
