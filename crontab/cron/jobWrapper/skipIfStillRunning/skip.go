package main

import (
	"log"
	"sync/atomic"
	"time"

	"github.com/robfig/cron/v3"
)

type skipJob struct {
	count int32
}

func (job *skipJob) Run() {
	atomic.AddInt32(&job.count, 1)
	log.Printf("%d: hello world\n", job.count)
	if atomic.LoadInt32(&job.count) == 1 {
		time.Sleep(2 * time.Second)
	}
}

func main() {
	c := cron.New()
	c.AddJob("@every 1s", cron.NewChain(cron.SkipIfStillRunning(cron.DefaultLogger)).Then(&skipJob{}))
	c.Start()

	time.Sleep(10 * time.Second)
}

// 2022/10/11 21:29:41 1: hello world
// 2022/10/11 21:29:44 2: hello world
// 2022/10/11 21:29:45 3: hello world
// 2022/10/11 21:29:46 4: hello world
// 2022/10/11 21:29:47 5: hello world
// 2022/10/11 21:29:48 6: hello world
// 2022/10/11 21:29:49 7: hello world
// 2022/10/11 21:29:50 8: hello world
